﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugConsoleView : MonoBehaviour
{
    private static DebugConsoleView instance = null;

    public Button compileButton;
    public InputField inputField;
    public Text viewPort;

    Compiler compiler = new Compiler();
    VirtualMachine vm = new VirtualMachine();


    public static DebugConsoleView Instance
    {
        get
        {
            return instance;
        }
    }

    // Use this for initialization
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }

        instance = this;
        DontDestroyOnLoad(this.gameObject);

        compileButton.onClick.AddListener(OnClick);
    }
    
    void OnClick()
    {
        List<Instruction> program;

        if (compiler.Compile(inputField.text, out program))
        {
            vm.Reset(program);
        }
    }

    void Update()
    {
        vm.RunStep();
    }
}
