﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BT_OR<T> : BT_Node<T> where T : class
{
    BT_Node<T> child1, child2;

    public BT_OR(BT_Conditional<T> c1, BT_Conditional<T> c2)
    {
        child1 = c1;
        child2 = c2;
    }

    public BT_OR(BT_Conditional<T> c1, BT_OR<T> c2)
    {
        child1 = c1;
        child2 = c2;
    }

    public BT_OR(BT_Conditional<T> c1, BT_AND<T> c2)
    {
        child1 = c1;
        child2 = c2;
    }

    public BT_OR(BT_OR<T> c1, BT_OR<T> c2)
    {
        child1 = c1;
        child2 = c2;
    }

    public BT_OR(BT_OR<T> c1, BT_AND<T> c2)
    {
        child1 = c1;
        child2 = c2;
    }

    public BT_OR(BT_AND<T> c1, BT_AND<T> c2)
    {
        child1 = c1;
        child2 = c2;
    }

    public override State OnUpdate()
    {
        return child1.Execute() == child2.Execute() ? child1.lastState : State.Success;
    }
}
