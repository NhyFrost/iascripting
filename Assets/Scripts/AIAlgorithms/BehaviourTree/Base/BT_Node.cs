﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BT_Node<T> where T : class // T hereda de class
{
    public string name;

    protected T blackboard;

    public State currentState;

    public State lastState;

    public State childState;

    public enum State { Sleep, InProgress, Success, Failure }

    public virtual void OnEnter() { }

    public abstract State OnUpdate();

    public virtual void OnExit() { }

    public void Reset()
    {
        OnExit();
        currentState = State.Sleep;
    }

    public virtual bool Validate()
    {
        return true;
    }

    public State Execute()
    {
        if (Validate())
        {
            if (currentState != State.InProgress)
            {
                OnEnter();
                currentState = State.InProgress;
            }

            lastState = currentState = OnUpdate();

            if (currentState != State.InProgress)
            {
                Reset();
            }
        }
        else
        {
            lastState = State.Failure;
            Reset();
        }

        return lastState;
    }
}
