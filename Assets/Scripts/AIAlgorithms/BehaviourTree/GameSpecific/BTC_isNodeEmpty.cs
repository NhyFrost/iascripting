﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTC_isNodeEmpty : BT_Conditional<GameManager>
{
    Unit unit;

    public BTC_isNodeEmpty(Unit u)
    {
        unit = u;
    }

    public override void OnEnter()
    {
        if (unit.gatheringNode != null && unit.gatheringNode.Type == ResourceSpot.ResourceType.Empty)
        {
            answer = true;
        }
    }
}
