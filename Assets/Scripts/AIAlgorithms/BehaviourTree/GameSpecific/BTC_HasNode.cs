﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTC_HasNode : BT_Node<GameManager>
{
    Unit unit;
    int Index;

    public BTC_HasNode(Unit u)
    {
        unit = u;
    }

    public override State OnUpdate()
    {

        if (unit.gatheringNode != null && unit.gatheringNode.Type != ResourceSpot.ResourceType.Empty)
        {
            unit.destination.Enqueue(unit.gatheringNode.transform.position);
            return State.Success;
        }
        else if (Index >= GameManager.Instance.gatheringPoints.Count)
        {
            Index = 0;
            return State.Failure;
        }

        unit.gatheringNode = GameManager.Instance.gatheringPoints[Index++];
        return State.InProgress;
    }
}
