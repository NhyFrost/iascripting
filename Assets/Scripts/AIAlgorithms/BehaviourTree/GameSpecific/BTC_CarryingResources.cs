﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTC_CarryingResources : BT_Conditional<GameManager>
{
    Unit unit;

    public BTC_CarryingResources(Unit u)
    {
        unit = u;
    }

    public override void OnEnter()
    {
        if (unit.carriedWeight > 0)
        {
            answer = true;
        }
    }

}
