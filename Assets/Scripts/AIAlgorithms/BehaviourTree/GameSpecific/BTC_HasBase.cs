﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTC_HasBase : BT_Node<GameManager>
{
    Unit unit;
    int Index;

    public BTC_HasBase(Unit u)
    {
        unit = u;
    }

    public override State OnUpdate()
    {
        if (unit.Base != null)
        {
            unit.destination.Enqueue(unit.Base.transform.position);
            return State.Success;
        }
        else if (Index >= GameManager.Instance.gatheringPoints.Count)
        {
            Index = 0;
            return State.Failure;
        }

        unit.Base = GameManager.Instance.bases[Index++];
        return State.InProgress;
    }
}
