﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTC_WeightCapacity : BT_Conditional<GameManager>
{
    Unit unit;

    public BTC_WeightCapacity(Unit u)
    {
        unit = u;
    }

    public override void OnEnter()
    {
        if (unit.carriedWeight >= unit.maxCapacity)
        {
            answer = true;
        }
    }
}
