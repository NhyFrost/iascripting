﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourTree : MonoBehaviour
{
    Unit u;
    BT_Node<GameManager> RootSelector;

    void Start()
    {
        u = GetComponent<Unit>();

        List<BT_Node<GameManager>> depositList = new List<BT_Node<GameManager>>();

        BT_Conditional<GameManager> isCarrying = new BTC_CarryingResources(u);
        BT_Conditional<GameManager> isNodeEmpty = new BTC_isNodeEmpty(u);
        BT_Conditional<GameManager> isUnitFull = new BTC_WeightCapacity(u);

        BT_AND<GameManager> AND = new BT_AND<GameManager>(isCarrying, isNodeEmpty);
        BT_OR<GameManager> OR = new BT_OR<GameManager>(isUnitFull, AND);

        BT_Node<GameManager> hasBase = new BTC_HasBase(u);
        BT_Node<GameManager> moveToBase = new MovementCommand(u);
        BT_Node<GameManager> deposit = new DepositCommand(u);



        depositList.Add(OR);
        depositList.Add(hasBase);
        depositList.Add(moveToBase);
        depositList.Add(deposit);

        List<BT_Node<GameManager>> gatherList = new List<BT_Node<GameManager>>();

        BT_Node<GameManager> hasNode = new BTC_HasNode(u);
        BT_Node<GameManager> moveToNode = new MovementCommand(u);
        BT_Node<GameManager> gather = new MiningCommand(u);

        gatherList.Add(hasNode);
        gatherList.Add(moveToNode);
        gatherList.Add(gather);


        List<BT_Node<GameManager>> selectorList = new List<BT_Node<GameManager>>();

        BT_Node<GameManager> DepositSequencer = new BT_Sequencer<GameManager>(depositList, "Deposit Sequencer");
        BT_Node<GameManager> GatherSequencer = new BT_Sequencer<GameManager>(gatherList, "Gather Sequencer");

        selectorList.Add(DepositSequencer);
        selectorList.Add(GatherSequencer);


        RootSelector = new BT_Selector<GameManager>(selectorList, "Root Selector");
    }

    void Update()
    {
        RootSelector.Execute();
    }
}
