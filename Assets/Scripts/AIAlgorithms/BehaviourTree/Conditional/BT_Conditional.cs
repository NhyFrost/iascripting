﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BT_Conditional<T> : BT_Node<T> where T : class
{
    protected bool answer = false;

    public override State OnUpdate()
    {
        return answer ? State.Success : State.Failure;
    }
}
