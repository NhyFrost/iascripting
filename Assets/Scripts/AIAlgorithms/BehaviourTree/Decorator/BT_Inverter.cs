﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BT_Inverter<T> : BT_Node<T> where T : class
{
    BT_Node<T> child;

    BT_Inverter(BT_Node<T> c)
    {
        child = c;
    }

    public override void OnEnter()
    {

    }

    public override State OnUpdate()
    {
        childState = child.Execute();

        if (childState == State.Success)
            return State.Failure;
        if (childState == State.Failure)
            return State.Success;

        return State.InProgress;
    }

    public override void OnExit()
    {

    }
}
