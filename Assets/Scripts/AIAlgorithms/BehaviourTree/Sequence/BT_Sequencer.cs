﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BT_Sequencer<T> : BT_Node<T> where T : class
{
    List<BT_Node<T>> children;
    int currentChild;

    public BT_Sequencer(List<BT_Node<T>> c, string n)
    {
        children = c;
        name = n;
    }

    public override void OnEnter()
    {
        currentChild = 0;
    }

    public override State OnUpdate()
    {
        childState = children[currentChild].Execute();

        if (childState == State.Success)
        {
            currentChild++;

            if (currentChild >= children.Count)
            {
                return State.Success;
            }
        }
        else if (childState == State.Failure)
        {
            return State.Failure;
        }

        return State.InProgress;
    }
}
