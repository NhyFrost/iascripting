﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BT_Selector<T> : BT_Node<T> where T : class
{
    List<BT_Node<T>> children;
    int currentChild;
    State currentChildState;

    public BT_Selector(List<BT_Node<T>> c, string n)
    {
        children = c;
        name = n;
    }

    public override void OnEnter()
    {
        Debug.Log(name + " INIT");
        currentChild = 0;
    }

    public override State OnUpdate()
    {
        childState = children[currentChild].Execute();

        if (childState == State.Failure)
        {
            currentChild++;

            if (currentChild >= children.Count)
            {
                return State.Failure;
            }
        }
        else if (childState == State.Success)
        {
            return State.Success;
        }

        return State.InProgress;
    }
}
