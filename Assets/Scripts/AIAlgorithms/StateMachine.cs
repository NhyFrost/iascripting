﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    public Unit unit;
    int GIndex;
    int BIndex;
    public SM SMstates;

    public enum SM
    {
        Idle,
        Deposit,
        Gather,
        Combat
    }

    private void Update()
    {
        switch (SMstates)
        {
            case SM.Idle:
                break;
            case SM.Deposit:
                DepositState();
                break;
            case SM.Gather:
                GatherState();
                break;
            case SM.Combat:
                break;
            default:
                break;
        }
    }

    void IdleState()
    {

    }

    void DepositState()
    {
        if (unit.BehaviourState != Unit.IA_State.Idle)
        {
            return;
        }

        if (unit.carriedWeight == 0)
        {
            SMstates = SM.Gather;
            unit.destination.Clear();
        }

        if (unit.Base != null)
        {
            unit.destination.Enqueue(unit.Base.transform.position);
            BT_Node<GameManager> pfCMD = new MovementCommand(unit);
            GameManager.Instance.unit.GetComponent<CommandManager>().AddCommand(pfCMD);
            unit.Base.Action(unit);
        }
        else
        {
            if (BIndex >= GameManager.Instance.bases.Count)
            {
                SMstates = SM.Idle;
            }

            unit.Base = GameManager.Instance.bases[BIndex++];
        }


    }

    void GatherState()
    {
        if (unit.BehaviourState != Unit.IA_State.Idle)
        {
            return;
        }

        if (unit.carriedWeight == unit.maxCapacity)
        {
            SMstates = SM.Deposit;
            unit.destination.Clear();
            return;
        }

        if (unit.gatheringNode != null && unit.gatheringNode.Type != ResourceSpot.ResourceType.Empty)
        {
            unit.destination.Enqueue(unit.gatheringNode.transform.position);
            BT_Node<GameManager> pfCMD = new MovementCommand(unit);
            GameManager.Instance.unit.GetComponent<CommandManager>().AddCommand(pfCMD);
            unit.gatheringNode.Action(unit);
        }
        else
        {
            if (GIndex >= GameManager.Instance.gatheringPoints.Count)
            {
                SMstates = SM.Idle;
            }

            unit.gatheringNode = GameManager.Instance.gatheringPoints[GIndex++];
        }
    }

    void CombatState()
    {

    }

}