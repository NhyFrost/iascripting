﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : IHeapItem<Node>
{
    public Node parent;
    public int gridX;
    public int gridY;
    public bool walkable;
    public bool resource;
    public Vector3 worldPosition;
    public bool heuristics = true;
    public int movementPenalty;

    public int moveCost;
    public int heuristicCost;
    int heapIndex;

    public NodeType Ntype;

    public enum NodeType
    {
        Normal,
        Resource,
        Base
    }

    public Node(bool _walkable, NodeType _type, Vector3 _worldPos, int _gridX, int _gridY, int _movementPenalty = 0)
    {
        walkable = _walkable;
        Ntype = _type;
        worldPosition = _worldPos;
        gridX = _gridX;
        gridY = _gridY;
        movementPenalty = _movementPenalty;
    }

    public int accumulatedCost
    {
        get
        {
            return moveCost + heuristicCost;
        }
    }

    public int HeapIndex
    {
        get { return heapIndex; }
        set { heapIndex = value; }
    }

    public int CompareTo(Node nodeToCompare)
    {
        int compare = moveCost.CompareTo(nodeToCompare.moveCost);
        if (compare == 0 || heuristics)
        {
            compare = heuristicCost.CompareTo(nodeToCompare.heuristicCost);
        }
        return -compare;
    }
}
