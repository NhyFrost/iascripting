﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinder : MonoBehaviour
{
    Queue<Node> nodesBreadthFirst;
    Stack<Node> nodesDepthFirst;
    List<Node> nodesDijkstra;
    List<Node> nodesAStar;
    HashSet<Node> closedNodes;

    public PathFinderState currentState;

    public enum PathFinderState
    {
        Waiting,
        Finding,
        Reach
    }

    void StartOpenNodes(Node startN)
    {
        switch (type)
        {
            case PathfinderType.BreadthFirst:
                nodesBreadthFirst = new Queue<Node>();
                nodesBreadthFirst.Enqueue(startN);
                break;
            case PathfinderType.DepthFirst:
                nodesDepthFirst = new Stack<Node>();
                nodesDepthFirst.Push(startN);
                break;
            case PathfinderType.Dijkstra:
                nodesDijkstra = new List<Node>();
                nodesDijkstra.Add(startN);
                break;
            case PathfinderType.AStar:
                nodesAStar = new List<Node>();
                nodesAStar.Add(startN);
                break;
            default:
                break;
        }

        closedNodes = new HashSet<Node>();

        currentState = PathFinderState.Finding;
    }

    public Vector3[] Find(Unit unit, Vector3 targetPos)
    {
        Node startNode = GameManager.Instance.NodeFromWorldPoint(unit.transform.position);
        Node targetNode = GameManager.Instance.NodeFromWorldPoint(targetPos);

        StartOpenNodes(startNode);

        if (startNode.walkable && targetNode.walkable)
        {
            while (true)
            {
                Node currentNode = GetNode();

                if (currentNode == null)
                {
                    Debug.LogWarning("Path not found");
                    return null;
                }

                if (currentNode == targetNode)
                {
                    return RetracePath(startNode, targetNode);
                }

                closedNodes.Add(currentNode);
                GetAdjacent(currentNode, targetNode);
            }
        }

        return null;

       /* yield return null;

        unit.path = RetracePath(startNode, targetNode);

        if (pathSucces)
        {
            waypoints = RetracePath(startNode, targetNode);

            if (targetNode.Ntype == Node.NodeType.Resource)
            {
                BT_Node<T> mCMD = new MiningCommand(unit, unit.gatheringNode);
                GameManager.Instance.unit.GetComponent<CommandManager>().AddCommand(mCMD);
            }
            if (targetNode.Ntype == Node.NodeType.Base)
            {
                BT_Node<T> mCMD = new DepositCommand(unit, unit.Base);
                GameManager.Instance.unit.GetComponent<CommandManager>().AddCommand(mCMD);
            }
            currentState = PathFinderState.Reach;
        }*/

    }

    Vector3[] RetracePath(Node startNode, Node endNode)
    {
        List<Node> path = new List<Node>();
        Node currentNode = endNode;

        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }

        if (path.Count <= 1 || path == null) return null;

        Vector3[] waypoints = SimplifyPath(path);
        System.Array.Reverse(waypoints);
        return waypoints;

    }

    Vector3[] SimplifyPath(List<Node> path)
    {
        List<Vector3> waypoints = new List<Vector3>();
        Vector2 directionOld = new Vector2(path[0].gridX - path[1].gridX, path[0].gridY - path[1].gridY);
        waypoints.Add(path[0].worldPosition);

        for (int i = 1; i < path.Count; i++)
        {
            Vector2 directionNew = new Vector2(path[i - 1].gridX - path[i].gridX, path[i - 1].gridY - path[i].gridY);
            if (directionNew != directionOld)
            {
                waypoints.Add(path[i].worldPosition);
            }
            directionOld = directionNew;
        }
        return waypoints.ToArray();
    }


    public PathfinderType type;

    public enum PathfinderType
    {
        BreadthFirst,
        DepthFirst,
        Dijkstra,
        AStar
    }

    Node GetNode()
    {
        switch (type)
        {
            case PathfinderType.BreadthFirst:
                if (nodesBreadthFirst.Count > 0)
                    return nodesBreadthFirst.Dequeue();
                break;
            case PathfinderType.DepthFirst:
                if (nodesDepthFirst.Count > 0)
                    return nodesDepthFirst.Pop();
                break;
            case PathfinderType.Dijkstra:
                if (nodesDijkstra.Count > 0)
                {
                    Node n = nodesDijkstra[0];

                    foreach (var node in nodesDijkstra)
                    {
                        if (n.moveCost > node.moveCost)
                        {
                            n = node;
                        }
                    }
                    nodesDijkstra.Remove(n);
                    return n;
                }
                break;
            case PathfinderType.AStar:
                if (nodesAStar.Count > 0)
                {
                    Node n = nodesAStar[0];

                    
                    foreach (var node in nodesAStar)
                    {
                        if (node.accumulatedCost < n.accumulatedCost || node.accumulatedCost == n.accumulatedCost && node.heuristicCost < n.heuristicCost)
                        {
                            n = node;
                        }
                    }

                    nodesAStar.Remove(n);
                    return n;
                }
                break;
            default:
                break;
        }


        Debug.Log("NO HAY ABIERTOS");
        return null;
    }

    void GetAdjacent(Node n, Node targetN)
    {
        var adjacents = GameManager.Instance.GetNodeAdjacents(n);

        int newMoveCost;

        for (int i = 0; i < adjacents.Count; i++)
        {
            if (!adjacents[i].walkable) continue;

            switch (type)
            {
                case PathfinderType.BreadthFirst:
                    if (!nodesBreadthFirst.Contains(adjacents[i]) && !closedNodes.Contains(adjacents[i]))
                    {
                        adjacents[i].parent = n;
                        nodesBreadthFirst.Enqueue(adjacents[i]);
                    }
                    break;
                case PathfinderType.DepthFirst:
                    if (!nodesDepthFirst.Contains(adjacents[i]) && !closedNodes.Contains(adjacents[i]))
                    {
                        adjacents[i].parent = n;
                        nodesDepthFirst.Push(adjacents[i]);
                    }
                    break;
                case PathfinderType.Dijkstra:
                    if (!closedNodes.Contains(adjacents[i]))
                    {
                        newMoveCost = n.moveCost + adjacents[i].movementPenalty;
                        if (newMoveCost < adjacents[i].moveCost || !nodesDijkstra.Contains(adjacents[i]))
                        {
                            adjacents[i].moveCost = newMoveCost;
                            adjacents[i].parent = n;

                            if (!nodesDijkstra.Contains(adjacents[i]))
                            {
                                nodesDijkstra.Add(adjacents[i]);
                            }
                            /*else
                            {
                                nodesDijkstra.UpdateItem(adjacents[i]);
                            }*/
                        }
                    }
                    break;
                case PathfinderType.AStar:
                    if (!closedNodes.Contains(adjacents[i]))
                    {
                        newMoveCost = n.moveCost + GetDistance(n, adjacents[i]) + adjacents[i].movementPenalty;
                        if (newMoveCost < adjacents[i].moveCost || !nodesAStar.Contains(adjacents[i]))
                        {
                            adjacents[i].moveCost = newMoveCost;
                            adjacents[i].heuristicCost = GetDistance(adjacents[i],targetN);
                            adjacents[i].parent = n;

                            if (!nodesAStar.Contains(adjacents[i]))
                            {
                                nodesAStar.Add(adjacents[i]);
                            }
                            /*else
                            {
                                nodesAStar.UpdateItem(adjacents[i]);
                            }*/
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public int GetDistance(Node nodeA, Node nodeB)
    {
        int dstX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int dstY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

        if (dstX > dstY)
            return 14 * dstY + 10 * (dstX - dstY); // 10 es la distancia nodo horizontal/diagonal 1 * 10
        return 14 * dstX + 10 * (dstY - dstX);     // 14 es la distancia nodo diagonal raiz de 2 * 10 =~ 14
    }

    public void SetBreadthFirst() { type = PathfinderType.BreadthFirst; }
    public void SetDepthFirst() { type = PathfinderType.DepthFirst; }
    public void SetDijkstra() { type = PathfinderType.Dijkstra; }
    public void SetAStar() { type = PathfinderType.AStar; }
}
