﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector : MonoBehaviour
{
    public Pathfinder pf;

    void Update()
    {
        /*
        RaycastHit mousePos = new RaycastHit();

        bool targeting = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out mousePos);

        if (targeting)
        {
            if (GameManager.Instance.NodeFromWorldPoint(mousePos.point).walkable)
            {
                pf.StartFind(GameManager.Instance.unit, mousePos.point);
            }
        }
        */
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo = new RaycastHit();

            bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);

            if (hit)
            {
                if (GameManager.Instance.NodeFromWorldPoint(hitInfo.point).walkable)
                {
                    CommandManager unitCM = GameManager.Instance.unit.GetComponent<CommandManager>();
                    StateMachine unitSM = GameManager.Instance.unit.GetComponent<StateMachine>();

                    if (unitSM.SMstates != StateMachine.SM.Idle)
                    {
                        unitCM.commands.Clear();
                        unitCM.actualCommand = null;
                        GameManager.Instance.unit.destination.Clear();
                        GameManager.Instance.unit.GetComponent<StateMachine>().SMstates = StateMachine.SM.Idle;
                    }

                    GameManager.Instance.unit.destination.Enqueue(hitInfo.point);
                    BT_Node<GameManager> pfCMD = new MovementCommand(GameManager.Instance.unit);
                    unitCM.AddCommand(pfCMD);


                    if (hitInfo.collider.gameObject.GetComponent<Structure>())
                    {
                        hitInfo.collider.gameObject.GetComponent<Structure>().Action(GameManager.Instance.unit);
                    }
                }
            }
            else
                Debug.Log("No hit");
        }
    }
}
