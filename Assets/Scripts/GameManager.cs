﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Unit unit;
    [HideInInspector]
    public Pathfinder pf;
    public LayerMask unwalkableMask;
    public LayerMask resourceMask;
    public LayerMask baseMask;
    public List<Base> bases;
    public List<ResourceSpot> gatheringPoints;
    public Vector2 gridWorldSize;
    public float nodeRadius;
    Node[,] grid;
    float nodeDiameter;
    public GameObject view;
    public TerrainType[] Terrains;
    LayerMask walkableMask;
    public List<Node> path;
    public bool SeeGrid;
    public Node destNode;
    Dictionary<int, int> walkableRegionsDictionary = new Dictionary<int, int>(); // para revisar mas rapido
    float currentRadius;

    int gridSizeX, gridSizeY;

    private static GameManager instance = null;

    public static GameManager Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }

        instance = this;
        DontDestroyOnLoad(this.gameObject);

        bases = new List<Base>();
        gatheringPoints = new List<ResourceSpot>();

        pf = GetComponent<Pathfinder>();
        nodeDiameter = nodeRadius * 2;
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);

        foreach (var type in Terrains)
        {
            walkableMask.value |= type.terrainMask.value;
            walkableRegionsDictionary.Add((int)Mathf.Log(type.terrainMask.value, 2), type.movementPenalty); // Se saca el logaritmo del valor del layer para pasarlo de numero binario a entero y pasarlo al diccionario como indice
        }

        currentRadius = nodeRadius;

        CreateGrid();
    }

    public int MaxSize
    {
        get
        {
            return gridSizeX * gridSizeY;
        }
    }


    void CreateGrid()
    {
        grid = new Node[gridSizeX, gridSizeY];
        Vector3 worldBottomLeft = transform.position - Vector3.right * gridWorldSize.x / 2 - Vector3.forward * gridWorldSize.y / 2;

        for (int x = 0; x < gridSizeX; x++)
        {
            for (int y = 0; y < gridSizeY; y++)
            {
                Vector3 worldPoint = worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.forward * (y * nodeDiameter + nodeRadius);
                bool walkable = !(Physics.CheckSphere(worldPoint, nodeRadius, unwalkableMask));

                Node.NodeType type = Node.NodeType.Normal;

                if (Physics.CheckSphere(worldPoint, nodeRadius, resourceMask))
                {
                    type = Node.NodeType.Resource;
                }
                else if (Physics.CheckSphere(worldPoint, nodeRadius, baseMask))
                {
                    type = Node.NodeType.Base;
                }


                int movementPenalty = 0;

                if (walkable)
                {
                    Ray ray = new Ray(worldPoint + Vector3.up * 50, Vector3.down);
                    RaycastHit hit;

                    if (Physics.Raycast(ray, out hit, 100, walkableMask))
                    {
                        walkableRegionsDictionary.TryGetValue(hit.collider.gameObject.layer, out movementPenalty);
                    }
                }

                grid[x, y] = new Node(walkable, type, worldPoint, x, y, movementPenalty);
            }
        }
    }

    public List<Node> GetNodeAdjacents(Node n)
    {
        List<Node> adj = new List<Node>();

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                    continue;

                int checkX = n.gridX + x;
                int checkY = n.gridY + y;

                if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY)
                {
                    adj.Add(grid[checkX, checkY]);
                }
            }
        }

        return adj;

    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(gridWorldSize.x, 1, gridWorldSize.y));

        if (grid != null)
        {
            Node unitNode = NodeFromWorldPoint(unit.gameObject.transform.position);

            foreach (Node n in grid)
            {

                if (destNode != null)
                {
                    Gizmos.color = Color.green;
                    Gizmos.DrawCube(destNode.worldPosition, Vector3.one * (nodeDiameter - 0.2f));
                }

                Gizmos.color = (n.walkable) ? Color.white : Color.red;

                if (unitNode == n)
                {
                    Gizmos.color = Color.cyan;
                }

                if (!n.walkable || SeeGrid)
                {
                    Gizmos.DrawCube(n.worldPosition, Vector3.one * (nodeDiameter - 0.2f));
                }
            }
        }
    }

    public Node NodeFromWorldPoint(Vector3 worldPosition)
    {
        float percentX = Mathf.Clamp01((worldPosition.x / gridWorldSize.x) + 0.5f);
        float percentY = Mathf.Clamp01((worldPosition.z / gridWorldSize.y) + 0.5f);

        int x = Mathf.RoundToInt((gridSizeX - 1) * percentX);
        int y = Mathf.RoundToInt((gridSizeY - 1) * percentY);

        return grid[x, y];
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.BackQuote))
            view.SetActive(!view.activeSelf);

        if (currentRadius != nodeRadius)
        {
            currentRadius = nodeRadius;
            CreateGrid();
        }
    }

    [System.Serializable]
    public class TerrainType
    {
        public LayerMask terrainMask;
        public int movementPenalty;
    }

}
