﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    [HideInInspector]
    public Vector3[] path;
    public int targetIndex = 0;
    public ResourceSpot gatheringNode;
    public Base Base;
    public CommandManager cmdManager;
    public StateMachine SM;
    public float speed = 20;
    public ResourceSpot.ResourceType ResourceType;
    public int maxCapacity;
    public int carriedWeight;
    public Queue<Vector3> destination = new Queue<Vector3>();

    public IA_State BehaviourState;

    public enum IA_State
    {
        Idle,
        Moving,
        Mining,
        Depositing
    }

    private void Start()
    {
        cmdManager = GetComponent<CommandManager>();
        SM = GetComponent<StateMachine>();
    }

    private void Update()
    {
        if (GetComponent<CommandManager>().actualCommand == null)
        {
            BehaviourState = IA_State.Idle;
        }

        switch (BehaviourState)
        {
            case IA_State.Idle:
                print("Idleo");
                break;
            case IA_State.Moving:
                print("Mueveo");
                break;
            case IA_State.Mining:
                print("Mineo: "+ ResourceType.ToString() +" Total: "+ carriedWeight);
                break;
            default:
                break;
        }
    }



    private void OnDrawGizmos()
    {
        if (path != null)
        {
            for (int i = targetIndex; i < path.Length; i++)
            {
                Gizmos.color = Color.black;
                Gizmos.DrawCube(path[i], Vector3.one);

                if (i == targetIndex)
                {
                    Gizmos.DrawLine(transform.position, path[i]);
                }
                else
                {
                    Gizmos.DrawLine(path[i - 1], path[i]);
                }
            }
        }
    }


}
