﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepositCommand : BT_Node<GameManager>
{
    Unit unit;
    Base b;

    public DepositCommand(Unit u)
    {
        unit = u;
    }

    public override void OnEnter()
    {
        b = unit.Base;
    }

    public override State OnUpdate()
    {
        if (unit.ResourceType == ResourceSpot.ResourceType.Empty)
            return State.Success;

        int depositTick = unit.carriedWeight; //en este caso deposita todo de una

        b.Deposit(depositTick, unit.ResourceType);

        unit.carriedWeight -= depositTick;


        if (unit.carriedWeight <= 0)
        {
            unit.carriedWeight = 0;
            unit.ResourceType = ResourceSpot.ResourceType.Empty;
            return State.Success;
        }

        unit.BehaviourState = Unit.IA_State.Depositing;
        return State.InProgress;

    }

    public override void OnExit()
    {
        
    }
}
