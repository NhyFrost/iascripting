﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandManager : MonoBehaviour
{
    public BT_Node<GameManager> actualCommand;
    public Queue<BT_Node<GameManager>> commands = new Queue<BT_Node<GameManager>>();

    public void AddCommand(BT_Node<GameManager> command)
    {
        commands.Enqueue(command);
    }

    void Update()
    {
        if (actualCommand != null)
        {
            BT_Node<GameManager>.State state = actualCommand.Execute();

            if (state == BT_Node<GameManager>.State.Success)
            {
                actualCommand = null;
            }

            if (state == BT_Node<GameManager>.State.Failure)
            {
                actualCommand = null;
                commands.Clear();
            }
        }
        else if (commands.Count > 0)
        {
            actualCommand = commands.Dequeue();
            actualCommand.OnEnter();
        }
    }
}
