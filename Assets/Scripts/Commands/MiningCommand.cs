﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiningCommand : BT_Node<GameManager>
{
    Unit unit;
    ResourceSpot resource;
    
    public MiningCommand(Unit u)
    {
        unit = u;
    }

    public override void OnEnter()
    {
        resource = unit.gatheringNode;
        unit.ResourceType = resource.Type;
    }

    public override State OnUpdate()
    {
        if (resource.Type == ResourceSpot.ResourceType.Empty)
            return State.Failure;

            unit.carriedWeight += resource.Gather();

            if (unit.carriedWeight >= unit.maxCapacity)
            {
                unit.carriedWeight = unit.maxCapacity;
                return State.Success;
            }

        unit.BehaviourState = Unit.IA_State.Mining;
        return State.InProgress;

    }
}
