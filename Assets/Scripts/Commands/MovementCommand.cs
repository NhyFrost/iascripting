﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementCommand : BT_Node<GameManager>
{
    Vector3[] path;
    Unit unit;
    Vector3 targetPos;
    float movSpeed;
    int targetIndex;
    Vector3 currentPoint;

    public MovementCommand(Unit u)
    {
        unit = u;
    }

    public override void OnEnter()
    {
        if (unit.destination.Count > 0)
        {
            path = GameManager.Instance.pf.Find(unit, unit.destination.Dequeue()); //podria usar blackboard si GameManager no fuera singleton

            if (path != null)
            {
                unit.path = path;
                unit.targetIndex = 0;
                currentPoint = path[0];
                targetIndex = 0;
                movSpeed = unit.speed;
            }
        }
    }

    public override State OnUpdate()
    {
        if (path == null)
        {
            return State.Failure;
        }

        if (GameManager.Instance.NodeFromWorldPoint(unit.transform.position) == GameManager.Instance.NodeFromWorldPoint(currentPoint))
        {
            targetIndex++;
            unit.targetIndex++;

            if (targetIndex >= path.Length)
            {
                unit.path = null;
                unit.targetIndex = 0;
                return State.Success;
            }

            currentPoint = path[targetIndex];
        }

        unit.transform.position = Vector3.MoveTowards(unit.transform.position, currentPoint, movSpeed * Time.deltaTime);

        unit.BehaviourState = Unit.IA_State.Moving;
        return State.InProgress;
    }
}
