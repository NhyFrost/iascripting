﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoonSharp.Interpreter;

public class SetUnitSpeed : MonoBehaviour {

    Script script;

    public void Awake()
    {
        script = new Script();
        
    }

    // Use this for initialization
    void Start()
    {
        string scriptCode = @"    
			-- defines the update function
			function SetSpeed(x)
				obj.SetSpeed(x)
			end";


        UserData.RegisterType<SetUnitSpeed>();

        DynValue obj = UserData.Create(this);

        script.Globals.Set("obj", obj);

        script.DoString(scriptCode);
    }

    public void SetSpeed(float x)
    {
        GameManager.Instance.unit.speed += x;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            script.Call(script.Globals["SetSpeed"], Time.deltaTime* 10);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            script.Call(script.Globals["SetSpeed"], Time.deltaTime* -10);
        }

    }
}
