﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceSpot : Structure
{
    public int ResourceQuantity;
    public ResourceType Type;
    float timer = 1;
    public int gatherTick;

    public enum ResourceType
    {
        Empty,
        Gold,
        Silver,
        Copper
    }

    private void Start()
    {
        GameManager.Instance.gatheringPoints.Add(this);
    }

    public override void Action(Unit u)
    {
        if (u.ResourceType == ResourceType.Empty || u.ResourceType == Type)
        {
            u.gatheringNode = this;
            u.SM.SMstates = StateMachine.SM.Gather;
            BT_Node<GameManager> cmd = new MiningCommand(u);
            u.cmdManager.AddCommand(cmd);
        }

    }

    public int Gather()
    {
        timer += Time.deltaTime;
        int gatheredResource = 0;

        if (timer > 1)
        {
            gatheredResource = gatherTick;
            ResourceQuantity -= gatherTick;

            if (ResourceQuantity <= 0)
            {
                Type = ResourceType.Empty;
                gatheredResource += ResourceQuantity;
                ResourceQuantity = 0;
            }
            timer = 0;
        }

        return gatheredResource;
    }

}
