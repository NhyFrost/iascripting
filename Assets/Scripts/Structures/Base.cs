﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Base : Structure
{
    public int goldQuantity;
    public int copperQuantity;
    public int silverQuantity;
    public float tickTime;
    float timer = 0;

    public void Start()
    {
        GameManager.Instance.bases.Add(this);
    }

    public override void Action(Unit u)
    {
        u.Base = this;
        if (u.carriedWeight > 0)
        {
            BT_Node<GameManager> cmd = new DepositCommand(u);
            u.cmdManager.AddCommand(cmd);
        }
        else
        {
            //repair, por ejemplo
        }

    }

    public void Deposit(int resourceQuantity, ResourceSpot.ResourceType type)
    {
        timer += Time.deltaTime;

        if (timer > tickTime)
        {
            switch (type)
            {
                case ResourceSpot.ResourceType.Empty:
                    Debug.LogWarning("NO PUEDE SER EMPTY");
                    break;
                case ResourceSpot.ResourceType.Gold:
                    goldQuantity += resourceQuantity;
                    break;
                case ResourceSpot.ResourceType.Silver:
                    silverQuantity += resourceQuantity;
                    break;
                case ResourceSpot.ResourceType.Copper:
                    copperQuantity += resourceQuantity;
                    break;
                default:
                    break;
            }
            timer = 0;
        }
    }
}
