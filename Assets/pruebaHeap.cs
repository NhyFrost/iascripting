﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pruebaHeap : MonoBehaviour
{
    public Heap<Node> nodes;
    int xPos, zPos;
    GameObject prefab;

    private void Start()
    {
        nodes = new Heap<Node>(100);
        xPos = zPos = 0;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            AddNode();
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            ShowRemovedCost();
        }
    }

    void AddNode()
    {
        Node n = new Node(true, Node.NodeType.Normal, new Vector3(xPos, 0, zPos), xPos, zPos);
        //Instantiate();
        nodes.Add(n);
        xPos++;
        n.moveCost = Random.Range(0,100);

        Debug.Log(n.moveCost);
    }

    void ShowRemovedCost()
    {
        Debug.Log(nodes.RemoveFirst().moveCost);
    }
}
